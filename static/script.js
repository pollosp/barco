function fetchSailingData(event) {
    event.preventDefault();

    var form = document.getElementById('sailingForm');
    var submitButton = form.querySelector('input[type="submit"]');
    var loadingMessage = document.getElementById('loadingMessage');
    var resultDisplay = document.getElementById('result');

    submitButton.disabled = true;
    loadingMessage.style.display = 'block'; // Show the loading message
    resultDisplay.innerHTML = ''; // Clear previous results

    var url = '/howhard?' + new URLSearchParams(new FormData(form)).toString();

    fetch(url)
        .then(response => response.json())
        .then(data => {
            var riskValue = data.sailing_risk || (data.data && data.data.sailing_risk) || 'No risk data';
            var reason = data.reason || (data.data && data.data.reason) || 'No reason data';
            document.getElementById('result').innerHTML = "<span style='color: red; font-size: 24px;'>⛵️ SAILING RISK: </span>" +
                "<span style='color: red; font-size: 24px;'>" + riskValue + "</span><br></br>"+ "<span style='color: blue; font-size: 12px;'>🚢 Reasons:" + reason + "</span>";
        })
        .catch(error => {
            console.error('Error:', error);
            resultDisplay.innerText = 'Error fetching data';
        })
        .finally(() => {
            submitButton.disabled = false;
            loadingMessage.style.display = 'none'; // Hide the loading message
            form.reset();
        });

    return false;
}
