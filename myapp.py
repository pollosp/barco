from openai import OpenAI

from flask import Flask, request, Response, json, render_template

app = Flask(__name__)

def openAI(wind, direction, location, hours, waves, gust, boat_size):
    client = OpenAI()

    completion = client.chat.completions.create(
      model="gpt-4-1106-preview",
      response_format={ "type": "json_object" },
      messages=[
        {"role": "system", "content": "You are expert saling into the mediterrean sea and you sould give if the sail if going to be easy or hard in a scale of 1 to 10 where 1 is very easy and 10 is very hard depending on the weather conditions, hours of sailing and type of boat users are going to ask you. You must answer the number 1 to 10 as sailing_risk field in the json sort clear reason of why in a reason field in a json format with this schema { \"sailing_risk\": 8 ,\"reason\": } "},
        {"role": "user", "content": f"{wind} knts winds with gust of {gust} from {direction} in this location {location} , with an experience of sailing {hours} hours, {waves} meters waves, the sail boat we are going to use is {boat_size} meters long"}
      ]
    )
    client.close()
    return completion.choices[0].message.content


@app.route('/howhard', methods=['GET'])
def get_parameters():
    wind = request.args.get('wind')
    gust = request.args.get('gust')
    direction = request.args.get('direction')
    location = request.args.get('location')
    hours = request.args.get('hours')
    waves = request.args.get('waves')
    boat_size = request.args.get('boat_size')

    data = openAI(wind, direction, location, hours, waves, gust, boat_size)
    response = Response(
        response=data,
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/')
def index():
  default_values = {
        'wind': '10',
        'gust': '14',
        'direction': 'NW',
        'location': 'VALENCIA,SPAIN',
        'hours': '500',
        'waves': '1',
        'boat_size': '8'
    }
  return render_template('index.html', defaults=default_values)

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=8080,debug=True)
